# Configuração do Popup de Aceite - Portal do Aluno UBM

Instruções para implantação do recurso de aceite no portal do aluno.

**1)** Copie o seguinte código para no final do **index.html** no caminho Web/App/Edu/PortalEducacional/

    jQuery.support.cors = true;

    window.addEventListener('message', event => {
        if (event.origin.startsWith('http://intranet.ubm.br')) { 
            console.log(event.data); 
            window.location.reload();
        } else {
            return; 
        } 
    }); 

    function abrirUrlExterna(){
        $.ajax({
            url: "https://intra.ubm.br:8080/rm/api/TOTVSEducacional/GetKeySession",
            crossDomain: true,
            context: document.body,
            type: 'get',
            xhrFields:{withcredentials: true}
        }).done(function(data){    
            var RAALUNO = $(".raaluno").html();
            var arrRa = RAALUNO.split(":");
            var numberPattern = /\d+/g;
            var ra = arrRa[1].match( numberPattern ).join('');
            var url = "http://intranet.ubm.br/aceite/termos2021.php?RAaluno=" + ra;
            window.open(url, '_blank');
            //$('.fancybox-iframe').attr('src', url);
        });
    }

    function getRA(){
        var RAALUNO = $(".raaluno").html();
        console.log(RAALUNO);
        var arrRa = RAALUNO.split(":");
        var numberPattern = /\d+/g;
        if(arrRa[1].match( numberPattern ) != null){
            var ra = arrRa[1].match( numberPattern ).join('');
            console.log(arrRa[1]);
            console.log(ra);
            $.ajax({
                url: "consultara.asp?ra=" + ra,
                crossDomain: true,
                context: document.body,
                type: 'get',
                xhrFields:{withcredentials: true}
            }).done(function(response){   
                console.log(response);
                if(response=="1"){
                    $('#popup').click();
                }
            });
        }
    }

    (function(){
        var myApp = angular.module('myApp', []);
        myApp.controller('myController', function($scope, $timeout){
            //10 seconds delay
            $timeout( function(){
                getRA();
            }, 10000 ); 
        });
    })();

**2)** Copie o arquivo **consultara.asp** para a mesma pasta.

**Obs**

O código comentado:

    //$('.fancybox-iframe').attr('src', url);

Tem a função de carregar os temos do aceite dentro do mesmo popup que carrega o banner. Entretanto ele foi desativado pois o servidor do aceite não possuí certificado de segurança válido, isso afeta a experiência do usuário. Assim que possível, sugiro migrar o servidor do aceite para um que tenha o certificado de segurança válido para que a experiência do usuário seja mais fluída.

**SQL**

Abaixo seguem as consultas SQL utilizadas para validação do aceite.

**1)** Código que valida se o usuário já fez o aceite. Caso não tenha efetuado segue para a próxima consulta. (Essa consulta é efetuada na base de dados específica do aceite)

    SELECT AC.RA FROM ACEITEPREVIA AC WHERE AC.RA = '$RA' AND AC.SEMESTRE = '20201001642 AND Semestre = '20211''   

**2)** Código que valida se o usuário é elegível para o aceite. (Consulta efetuada na base de dados do RM)

    SELECT CODSTATUS, CODPERLET FROM CORPORERM.dbo.SMATRICPL
    INNER JOIN
    CORPORERM.dbo.SPLETIVO ON CORPORERM.dbo.SPLETIVO.CODCOLIGADA = CORPORERM.dbo.SMATRICPL.CODCOLIGADA AND SMATRICPL.IDPERLET = SPLETIVO.IDPERLET
    WHERE (
    CODSTATUS = 1 OR
    CODSTATUS = 3 OR
    CODSTATUS = 6 OR
    CODSTATUS = 11 OR
    CODSTATUS = 12 OR
    CODSTATUS = 53 OR
    CODSTATUS = 54 OR
    CODSTATUS = 82 OR
    CODSTATUS = 92) AND
    RA = '20201001642'AND
    CODPERLET = '20211' AND
    SPLETIVO.CODTIPOCURSO = 1
    ORDER BY DTMATRICULA DESC
    OFFSET 0 ROWS
    FETCH NEXT 1 ROW ONLY
    **FETCH NEXT** 1 **ROW ONLY**

**Obs:** As consultas SQL mencionadas acima forma configuradas implementadas no arquivo **checkaluno.php** no servidor onde e efetuado o aceite.
